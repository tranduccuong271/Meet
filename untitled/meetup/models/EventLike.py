from django.db import models
from django.contrib.auth.models import User
from meetup.models.Event import Event
from datetime import datetime


class EventLike(models.Model):
    idVisitor = models.ForeignKey(User, on_delete=models.CASCADE)
    idEvent = models.ForeignKey(Event, on_delete=models.CASCADE)
    timeLike = models.DateTimeField(default=datetime.now(), null=False)
    # unique_together = ("idVisitor", "idEvent")
    class Meta:
        indexes = [
            models.Index(fields=['idEvent']),
            models.Index(fields=['idEvent', 'idVisitor'])
        ]
