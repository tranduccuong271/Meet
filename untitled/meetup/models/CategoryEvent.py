from django.db import models
from meetup.models.Category import Category
from meetup.models.Event import Event
import datetime

class CategoryEvent(models.Model):
    idEvent = models.ForeignKey(Event, on_delete=models.CASCADE)
    idCategory = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        indexes = [
            models.Index(fields=['idCategory']),
            models.Index(fields=['idEvent'])
        ]