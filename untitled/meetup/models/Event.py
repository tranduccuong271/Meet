import datetime

from django.core.mail import send_mail
from django.db import models
from django.db.models import signals
from django.dispatch import receiver


class Event(models.Model):
    id = models.AutoField(primary_key=True, db_index=True)
    title = models.CharField(null=False, max_length=255, db_index=True)
    timeEvent = models.DateTimeField(null=False)
    endTimeEvent = models.DateTimeField(default=datetime.datetime.now())
    content = models.CharField(max_length=1000)
    location = models.CharField(null=False, max_length=1000)

    class Meta:
        indexes = [
            models.Index(fields=['title']),
            models.Index(fields=['id'])
        ]

_timeEvent = models.DateTimeField(null=False)
_location = models.CharField(null=False, max_length=1000)

@receiver(signals.pre_save, sender=Event)
def pre_save_event(sender, instance, **kwargs):
    _timeEvent = instance.timeEvent
    _location = instance.location

@receiver(signals.post_save, sender=Event)
def saved_event(sender, instance, created, **kwargs):
    from meetup.models.EventVisitor import EventVisitor
    if not created:
        if instance.timeEvent != _timeEvent or instance.location != _location:
            queryset = EventVisitor.objects.filter(idEvent__id=instance.id)
            list_email = []
            for eventVisitor in queryset:
                list_email.append(eventVisitor.idVisitor.email)
            send_mail('Change Information Event' + instance.title, message='Time: ' + str(instance.timeEvent) +' and location: ' + instance.location, from_email='20150537@student.hust.edu.vn', recipient_list=list_email, fail_silently=False)

