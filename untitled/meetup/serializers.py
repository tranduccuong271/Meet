from meetup.models.Event import Event
from meetup.models.Category import Category
from meetup.models.Comment import Comment
from meetup.models.EventImage import EventImage
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.request import Request
class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = ('id','title', 'timeEvent')

class EventDetailSerializer(serializers.HyperlinkedModelSerializer):
    images = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='url'
    )
    class Meta:
        model = Event
        fields = ('id','title', 'timeEvent', 'location', 'timeEvent', 'endTimeEvent', 'content', 'images')

class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'content', 'image')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name')


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    visitor = UserSerializer()
    class Meta:
        model = Comment
        fields = ('id', 'content', 'visitor', 'updated_at')

class EventImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EventImage
        fields = ('id', 'url', 'created_at')