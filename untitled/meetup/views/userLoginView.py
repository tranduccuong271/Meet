from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response



class userLoginSet(viewsets.ViewSet):
    @action(methods=['post'], detail=False)
    def login(self, request):
            username = request.POST.get('username', '')
            password = request.POST.get('password', '')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                print(type(user.id))
                request.session['user_id'] = user.id
                return Response('Loggedin')
            else:
                return Response('Notmatch')

    @action(methods=['post'], detail=False)
    def logout(self, request):
        logout(request)
        try:
            del request.session['user_id']
        except KeyError:
            pass
        return Response("You're logged out.")



