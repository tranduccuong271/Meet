from django.contrib import admin
from django.contrib.admin import AdminSite
from meetup.models.Comment import Comment
from meetup.models.EventVisitor import Event, EventVisitor
from meetup.models.EventLike import EventLike
from meetup.models.CategoryEvent import CategoryEvent
from meetup.models.EventImage import EventImage
from django.contrib.auth.models import  User
# Register your models here.
class MyAdminSite(AdminSite):
    site_header = 'Monty Python administration'

admin_site = MyAdminSite(name='myadmin')
admin_site.register(Event)
admin_site.register(CategoryEvent)
admin_site.register(User)
admin.site.login_template = 'meetup/login.html'
admin.site.register(Event)
admin.site.register(CategoryEvent)
admin.site.register(EventLike)
admin.site.register(EventVisitor)
admin.site.register(Comment)
admin.site.register(EventImage)
