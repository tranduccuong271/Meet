"""untitled URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from meetup.views import allEventView, allCategoryView, userLoginView, admin_login_view, eventVisitorView
from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin
from meetup.admin import admin_site



from django.urls import include, path
#
router = routers.SimpleRouter()
router.register(r'event', allEventView.AllEventSet, base_name='event')
router.register(r'visitor', eventVisitorView.EventVisitorSet, base_name='visitor')
router.register(r'allCategory', allCategoryView.AllCategorySet, base_name='allCategory')
router.register(r'user', userLoginView.userLoginSet, base_name='user')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    url(r'^myadmin/', admin_site.urls),
    url(r'^oauth/', include('social_django.urls', namespace='social')),  # <--
    # url(r'^myadmin/login',admin_login_view.login),
]
